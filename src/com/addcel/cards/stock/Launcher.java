package com.addcel.cards.stock;

import java.io.IOException;

import com.addcel.cards.stock.procesor.FileProcessor;

public class Launcher {

	public static void main(String[] args) throws IOException, Exception {
		FileProcessor processor = new FileProcessor();
		//orden de los args: archivo a leer, properties
		processor.processFile(args[0], args[1]);
	}

}
