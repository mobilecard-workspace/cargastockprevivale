package com.addcel.cards.stock.model.dto;

import com.addcel.utils.AddcelCrypto;

public class CardStockDTO {

	private String numero;
	private String ultimos;
	private String cv;
	private String vigencia;
	
	public CardStockDTO() {}

	public CardStockDTO(String numero, String ultimos, String cv, String vigencia) {
		super();
		this.numero = AddcelCrypto.encryptTarjeta(numero);
		this.ultimos = ultimos;
		this.cv = AddcelCrypto.encryptTarjeta(cv);
		this.vigencia = AddcelCrypto.encryptTarjeta(vigencia);
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getUltimos() {
		return ultimos;
	}

	public void setUltimos(String ultimos) {
		this.ultimos = ultimos;
	}

	public String getCv() {
		return cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	
}
