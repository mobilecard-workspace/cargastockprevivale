package com.addcel.cards.stock.utils;

import java.io.FileReader;
import java.util.Properties;

public class Utils {

	public static Properties cargarProperties(String ruta) throws Exception{
		Properties properties = new Properties();
		try {
			System.out.println("Cargando archivo de propiedades: "+ruta);
			properties.load(new FileReader(ruta));
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new Exception("Error al cargar el archivo properties");
		}
		return properties;
	}
}
