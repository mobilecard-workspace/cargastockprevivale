package com.addcel.cards.stock.db.interfaz;

import java.sql.ResultSet;
import java.util.Properties;

public interface DbAccesor {

	ResultSet ejecutarQuery(String query, Properties properties) throws Exception;
	void save(String query, Properties properties) throws Exception;
}
