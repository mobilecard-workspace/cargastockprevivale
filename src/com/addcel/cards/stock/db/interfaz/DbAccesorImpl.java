package com.addcel.cards.stock.db.interfaz;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import com.addcel.cards.stock.db.AccesoDb;

public class DbAccesorImpl implements DbAccesor {

	@Override
	public ResultSet ejecutarQuery(String query, Properties props) throws Exception {
		Statement comando = null;    
	    ResultSet resultado = null;
	    AccesoDb db = null;
		try {
			db = new AccesoDb();
			comando = db.iniciarConexion(props).createStatement();
			resultado = comando.executeQuery(query);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return resultado;
	}

	@Override
	public void save(String query, Properties props) throws Exception {
		Statement comando = null;
	    AccesoDb db = null;
		try {
			db = new AccesoDb();
			comando = db.iniciarConexion(props).createStatement();
			comando.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
