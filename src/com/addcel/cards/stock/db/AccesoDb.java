package com.addcel.cards.stock.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class AccesoDb {

	//Conexion a la base de datos
	static Connection conexion = null;
	
	public Connection iniciarConexion(Properties properties) throws Exception {
		try {
			//Recuperamos las propiedades que necesita la conexion
			String usuario = properties.getProperty("db.usuario.prod");
			String password = properties.getProperty("db.password.prod");
			String url = properties.getProperty("db.url.prod");
			
			conexion = DriverManager.getConnection(url.replace("#{user}", usuario).replace("#{password}", password));
		} catch (Exception e) {
			System.err.print("Error al iniciar la conexion a la BD");
			e.printStackTrace();
			throw e;
		}
		return conexion;
	}
	
	public static void cerrarConexion() throws Exception{
		try {
			if(conexion != null)
				conexion.close();
		} catch (Exception e) {
			e.printStackTrace();
            throw e;
		}
	}
}
