package com.addcel.cards.stock.db.dao;

import java.sql.ResultSet;
import java.util.Properties;

import com.addcel.cards.stock.db.AccesoDb;
import com.addcel.cards.stock.db.interfaz.DbAccesorImpl;
import com.addcel.cards.stock.model.dto.CardStockDTO;

public class CardStockDAO {

	private DbAccesorImpl interfaz;
	
	public void saveCard(Properties properties, CardStockDTO dto) throws Exception{
		String query;
		try {
			interfaz = new DbAccesorImpl();
			
			int id = getLastid(properties);
			query = properties.getProperty("query.insert.card").replace("${id}", Integer.toString(id+1)).replace("${numero}", dto.getNumero()).replace("${cv}", dto.getCv()).replace("${vig}", dto.getVigencia()).replace("${ultimos}", dto.getUltimos());
			
			interfaz.save(query, properties);
		} catch (Exception e) {
			System.err.print("Error al insertar la tarjeta: "+e.getMessage());
			//e.printStackTrace();
			throw e;
		}finally {
			AccesoDb.cerrarConexion();
		}
	}
	
	private int getLastid(Properties properties) throws Exception{
		ResultSet result = null;
		int lastId = 0;
		try {
			interfaz = new DbAccesorImpl();
			result = interfaz.ejecutarQuery(properties.getProperty("query.getlastid"), properties);
			if(result.first())
				lastId = result.getInt(1);
		} catch (Exception e) {
			System.err.print("Error al consultar el ultimo id: "+e.getMessage());
			//e.printStackTrace();
			throw e;
		}
		return lastId;
	}
}
