package com.addcel.cards.stock.procesor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import com.addcel.cards.stock.db.dao.CardStockDAO;
import com.addcel.cards.stock.model.dto.CardStockDTO;
import com.addcel.cards.stock.utils.Utils;

public class FileProcessor {

	public void processFile(String pathFile, String properties) throws IOException{
		System.out.println("Leyendo los registros del archivo: "+pathFile);
		Path path = Paths.get(pathFile);
		try (Stream<String> stream = Files.lines(path)){
				processCards(stream, properties);
			System.out.println("Tarjetas almacenadas en stock");
		} catch (IOException e) {
			System.out.println("Error al leer el archivo: "+e.getMessage());
			throw e;
		}
	}
	
	private void processCards(Stream<String> stream, String properties){
		CardStockDAO dao;
		try {
			Properties props = Utils.cargarProperties(properties);
			System.out.println("Cargando las tarjetas leídas a la BD");
			String separador = Pattern.quote(props.getProperty("card.separator").trim());
			dao = new CardStockDAO();
			stream.forEach(card -> {
				try {
					String[] cardParts = card.split(separador);
					String ultimos = cardParts[0].substring(cardParts[0].length()-4, cardParts[0].length());
					System.out.println("Guardando en stock la tarjeta con terminacion: "+ultimos);
					CardStockDTO dto = new CardStockDTO(cardParts[0], ultimos, cardParts[1], cardParts[2].replace("-", "/"));
					dao.saveCard(props, dto);
				} catch (Exception e) {
					System.out.println("Error al guardar la tarjeta en la BD: "+e.getMessage());
				}
			});
		} catch (Exception error) {
			System.out.println("Error al procesar las tarjetas: "+error.getMessage());
		}
	}
}
